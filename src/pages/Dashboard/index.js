import React from 'react';

import { Container } from './styles';

export default function Dashboard() {
  return (
    <Container>
      <header>
        <strong>Dashboard</strong>
      </header>
    </Container>
  );
}
