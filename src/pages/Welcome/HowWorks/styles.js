import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  width: 100%;
  max-width: 900px;
  align-items: center;
  flex-direction: column;
`;

export const Title = styled.h1`
    color: #fff;
    width: 100%;
    font-size: 18px;
    margin-top: 20%;
    margin-right: 244%;
`;

export const Content = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;

`;

export const ImageContent = styled.div`
    width: 500px;
    height: 260px;
    background: #fff;
    margin-left: 5%;
    margin-top: 3%;
`;

export const ParagraphyContent = styled.div`
    width: 500px;
    margin-left: 5%;
    margin-top: 1%;
    p {
        text-align: center;
        color: #fff;
        font-size: 12px;
    }

`;

