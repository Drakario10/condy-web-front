import React from 'react';

import { Container,  } from './styles';

import HowWorks from './HowWorks';

export default function Welcome() {
  return (
    <Container>
       <HowWorks />
    </Container>
  );
}
