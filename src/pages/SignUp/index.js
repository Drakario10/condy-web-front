import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { Form, Input } from '@rocketseat/unform';
import { toast } from 'react-toastify';
import * as Yup from 'yup';

//import logo from '../../assets/logo.svg';

import { signUpRequest } from '../../store/modules/auth/actions';

export default function SignUp() {
  const [step, setStep] = useState(1);

  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');
  const [password_question, setPassword_question] = useState('');
  const [password_answer, setPassword_answer] = useState('');

  const [namecond, setNamecond] = useState('');
  const [address, setAddress] = useState('');
  const [phone, setPhone] = useState('');

  const dispatch = useDispatch();

  function handleClick() {
    dispatch(signUpRequest(name, email, login, password, password_question, password_answer, namecond, address, phone));
  }

  function handleContinue() {
    setStep(2);
  }

  function handleBack() {
    setStep(1);
  }

  function handleContinue2() {
    setStep(3);
  }

  switch (step) {
    case 1:
      return (
        <Form onSubmit={handleContinue} style={{ marginTop: '50%' }}>
          <Input name="name" placeholder="Seu nome completo" value={name} onChange={(e) => {
            setName(e.target.value);
          }} />
          <Input name="email" type="email" placeholder="Seu e-mail" value={email} onChange={(e) => {
            setEmail(e.target.value);
          }} />
          <Input name="login" placeholder="Seu nome de usuário" value={login} onChange={(e) => {
            setLogin(e.target.value);
          }} />
          <Input name="password" type="password" placeholder="Sua senha secreta" value={password} onChange={(e) => {
            setPassword(e.target.value);
          }} />
          <Input name="address" placeholder="Endereço" value={address} onChange={(e) => {
            setAddress(e.target.value);
          }} />

          <button type="submit">Continuar</button>
          <Link to="/login">Cancelar</Link>
        </Form>
      )
    case 2:
      return (
        <Form  style={{ marginTop: '50%' }}>
          <Input name="namecond" placeholder="Nome do condominio" value={namecond} onChange={(e) => {
            setNamecond(e.target.value);
          }} />
          <Input name="address" placeholder="Endereço" value={address} onChange={(e) => {
            setAddress(e.target.value);
          }} />
          <Input name="phone" placeholder="Telefone do condominio" value={phone} onChange={(e) => {
            setPhone(e.target.value);
          }} />

          <button onClick={handleBack}>Voltar</button>
          <button onClick={handleClick}>Continuar</button>
          <Link to="/login">Cancelar</Link>
        </Form>
      )

  }
}
