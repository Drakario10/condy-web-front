import React from 'react';
import PropTypes from 'prop-types';

import { Wrapper, Content } from './styles';

import PrincipalHeader from '../../../components/PrincipalHeader';

export default function AuthLayout({ children }) {
  return (
    <Wrapper>
      <PrincipalHeader />
      <Content>
      {children}
      </Content>
    </Wrapper>
  );
}

AuthLayout.propTypes = {
  children: PropTypes.element.isRequired,
}
