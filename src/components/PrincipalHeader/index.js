import React from 'react';
import { Link } from 'react-router-dom';


//import logo from '../../assets/logo-purple.svg';

import { Container, Content, Center, Auth, AuthIcon } from './styles';

export default function PrincipalHeader() {
  return (
    <Container>
      <Content>
        <nav>
          {/* <img src={logo} alt="appCondy" />*/}
          <Link to="/">appCondy</Link>
        </nav>

        <aside>
          <Center>
            <div>
              <Link>
                <strong>
                  Como funciona
              </strong>
              </Link>
              <Link>
                <strong>
                  Preços
              </strong>
              </Link>
              <Link>
                <strong>
                  Sobre nós
              </strong>
              </Link>
              <Link>
                <strong>
                  Carreira
              </strong>
              </Link>
            </div>

          </Center>

          <Auth>
            <div>
              <AuthIcon />

              <Link to="/login">
                <strong>Login</strong>
              </Link>
            </div>
          </Auth>
        </aside>
      </Content>
    </Container>
  );
}
