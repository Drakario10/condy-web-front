import styled from 'styled-components';
import { lighten } from 'polished';
import { MdPerson } from 'react-icons/md';

export const Container = styled.div`
  background:  #fff;
  padding: 0 30px;
`;

export const Content = styled.div`
   height: 60px;
   max-width: 900px;
   margin: 0 auto;
   display: flex;
   justify-content: space-between;  
   align-items: center;

   nav {
       display: flex;
       align-items: center;

       img {
           margin-right: 20px;
           padding-right: 20px;
           border-right: 1px solid #eee; 
       }

       a {
           font-size: 20px;
           font-weight: bold;
           color: #00EB5A;
           transition: color 0.2s;

        &:hover {
            color: ${lighten(0.03, '#00EB5A')};
        }
       }

       
   }

   aside {
       display: flex;
       align-items: center;
   }
`;


export const Center = styled.div`
  display: flex;
  margin-left: 10px;
  div {
     text-align: center;
     margin-right: 10px;
     display: flex;
     align-items: center;
     justify-content: space-between;
     flex-direction: row;

      strong {
          display: block;
          color: #333;
          margin-right: 20px;
          transition: color 0.2s;

          &:hover {
            color: ${lighten(0.1, '#333')};
        }
      }

      a {
          margin-top: 2px;
      }

      
  }
`;

export const Auth = styled.div`
  display: flex;
  margin-left: 20px;
  padding-left: 20px;
  border-left: 1px solid #eee;

  div {
     text-align: center;
     margin-right: 10px;
     display: flex;
     align-items: center;
     justify-content: space-between;
     flex-direction: row;

      strong {
          display: block;
          color: #333;
          margin-left: 10px;
      }

      a {
          margin-top: 2px;
      }

      
  }
`;

export const AuthIcon = styled(MdPerson).attrs({
    size: 30
})`
     padding: 2px 2px 2px 2px;
     border-radius: 50%;
     border-color: #333;
     border: 1.5px solid #333;
`;